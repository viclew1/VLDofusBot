package fr.lewon.dofus.bot.handlers.movement

import fr.lewon.dofus.bot.sniffer.model.messages.move.MapComplementaryInformationsDataMessage

object MapComplementaryInformationsDataEventHandler :
    AbstractMapComplementaryInformationsDataEventHandler<MapComplementaryInformationsDataMessage>()