package fr.lewon.dofus.bot.util.network

interface GameSnifferListener {
    
    fun onListenStart()

    fun onListenStop()

}